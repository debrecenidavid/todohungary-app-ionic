const categoryFilters=[
    { name: "All Category" },
    { name: "Bar" },
    { name: "City" },
    { name: "Club" },
    { name: "Concert" },
    { name: "Culture" },
    { name: "Dining" },
    { name: "Education" },
    { name: "Food" },
    { name: "Music" },
    { name: "Pub" },
    { name: "Sport" },
    { name: "Youth" },
  ];
  

  export const CATEGORY_FILTERS = categoryFilters;