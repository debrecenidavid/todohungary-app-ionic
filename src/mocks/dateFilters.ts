import * as moment from 'moment';
moment.locale('hu');
const todayStartOffset = 6 * 3600000; // 6 hours
const now = moment().startOf('day').valueOf() + todayStartOffset;

let name={
    name: 'Today',
    startTs: moment().valueOf()>now ? now:moment().startOf('hour').valueOf(),
    endTs: moment().endOf('day').valueOf() + todayStartOffset,
};
const datefilters= [
    {
        name: 'Today',
        startTs: moment().valueOf()>now ? now:moment().startOf('hour').valueOf(),
        endTs: moment().endOf('day').valueOf() + todayStartOffset,
    },
    {
        name: 'Tomorrow',
        startTs: moment().add(1, 'day').startOf('day').valueOf() + todayStartOffset,
        endTs: moment().add(1, 'day').endOf('day').valueOf() + todayStartOffset,
    },
    {
        name: 'This week',
        startTs: now,
        endTs: moment().endOf('week').valueOf() + todayStartOffset,
    },
    {
        name: 'Next week',
        startTs: moment().add(1, 'week').startOf('week').valueOf() + todayStartOffset,
        endTs: moment().add(1, 'week').endOf('week').valueOf() + todayStartOffset,
    },
    {
        name: 'This month',
        startTs: now,
        endTs: moment().endOf('month').valueOf() + todayStartOffset,
    },
    {
        name: 'Next month',
        startTs: moment().add(1, 'month').startOf('month').valueOf() + todayStartOffset,
        endTs: moment().add(1, 'month').endOf('month').valueOf() + todayStartOffset,
    },
    {
        name: 'All Upcoming',
        startTs: now,
        endTs: +Infinity,
    },
]

export const DATE_FILTERS = datefilters;