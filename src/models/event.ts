export interface OfficalEvent{
    id:number,

    name:string,
    startTs:number,
    startDtg:Date,
    endTs:number,
    endDtg:Date,
    guests:number,
    imgurl:string,

    link:string,

    tags:string[],

    location:Location,

    formattedDate?:string


}

export interface LocationEv{
    name:string,
    lat:number,
    lng:number,

    events:OfficalEvent[],
}

export interface Location{
    name:string,
    lat:number,
    lng:number,
}