export interface RegisterForm{
    firstName:string,
    lastName:string,
    email:string,
    password:string,

    confirmPassword:string,

    date:Date,
    gender:string,
    terms:boolean,
}

export interface Profile{
    firstName:string,
    lastName:string,
    email:string,
    date:Date,
    gender:string,

}