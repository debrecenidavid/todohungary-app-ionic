import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegisterForm } from '../../types/types';

@Injectable()
export class UserProvider {

  constructor(public http: HttpClient) {
  }

  getUser(token:string){
        const httpOptions = {
          headers: new HttpHeaders({
            'Authorization': token
          })
        }
        return this.http.get('http://localhost:5000/api/users/current',httpOptions);
  }

  login(email:string,password:string){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    }
    return this.http.post('http://localhost:5000/api/users/login',JSON.stringify({email,password}),httpOptions);
}

register(data:RegisterForm){
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  }
  return this.http.post('http://localhost:5000/api/users/register',JSON.stringify(data),httpOptions);
}

}
