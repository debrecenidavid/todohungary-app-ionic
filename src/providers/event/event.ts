import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OfficalEvent } from '../../models/event';
import { DATE_FILTERS } from '../../mocks/dateFilters';

@Injectable()
export class EventProvider {

  private officalUri: string = 'http://18.197.214.54:8008/'
  constructor(public http: HttpClient) {
    console.log('Hello EventProvider Provider');
  }


  getOfficialEvents() {
    const args = [[47.42, 21.42], [47.62, 21.8]]
    const action = 'events';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }

    return this.http.post<OfficalEvent[]>(this.officalUri, JSON.stringify({ args, action }), httpOptions).toPromise();
  }

  getEventsForDates(events: OfficalEvent[], dateFilter: string): OfficalEvent[] {
    const { startTs, endTs } = DATE_FILTERS.filter(v => v.name === dateFilter)[0];
    return events.filter(event => event.startTs > startTs && event.startTs < endTs || event.endTs > startTs && event.endTs < endTs) || [];
  }

  getEventsForTags(events:OfficalEvent[], tag):OfficalEvent[] {
    return events.filter(event => event.tags.indexOf(tag) !== -1) || [];
  }

  getIconUrl(name) {
    switch (name) {
      case "Bar":
        return 'assets/markers/Bar.png'
      case "City":
        return 'assets/markers/City.png'
      case "Club":
        return 'assets/markers/Club.png'
      case "Concert":
        return 'assets/markers/Concert.png'
      case "Culture":
        return 'assets/markers/Culture.png'
      case "Dining":
        return 'assets/markers/Dining.png'
      case "Education":
        return 'assets/markers/Education.png'
      case "Food":
        return 'assets/markers/Food.png'
      case "Music":
        return 'assets/markers/Music.png'
      case "Pub":
        return 'assets/markers/Pub.png'
      case "Sport":
        return 'assets/markers/Sport.png'
      case "Youth":
        return 'assets/markers/Youth.png'
      default:
        return 'assets/markers/Youth.png';
    }
  }
}
