import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController, LoadingOptions } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  account: { name: string, terms: boolean, date: any, gender: string, email: string, password: string } = {
    name: '',
    email: '',
    password: '',
    date: {},
    gender: '',
    terms: false
  };

  passwordMatch = true;
  registerForm: FormGroup;

  translatedItems;

  loading;
  constructor(public nav: NavController,
    private storage: Storage,
    public translateService: TranslateService,
    public termsAlert: AlertController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private userProvider: UserProvider, ) {

    translateService.get(["TERMS", "ACCEPT","MESSAGE_REGISTER_SUCCES", "MESSAGE_ERROR",
      "DECLINE", "TERMS_TEXT", "SAVE"]).subscribe(val => {
        this.translatedItems = val;
      })

    this.registerForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      terms: new FormControl(false, Validators.required),
    }, { validators: this.checkPasswords })
  }

  checkPasswords(group: FormGroup) {
    let pass = group.value.password;
    let confirmPass = group.value.confirmPassword;
    group.get('confirmPassword').setErrors(pass === confirmPass ? null : { notSame: true })
    return pass === confirmPass ? null : { notSame: true }

  }
  presentLoading() {
    const opts: LoadingOptions = {
      content: 'Please wait...',
      dismissOnPageChange: true
    }
    this.loading = this.loadingCtrl.create(opts)
    this.loading.present()
  }
  createToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top',
      cssClass: 'dark-trans',
      closeButtonText: 'OK',
      showCloseButton: true
    });
    toast.present();
  }
  async register() {
    this.presentLoading();
    this.userProvider.register(this.registerForm.value)
      .subscribe(
        (res: any) => {
          this.storage.set('loginDetails', { email: this.registerForm.value.email, password: this.registerForm.value.password })
          this.createToast(this.translatedItems["MESSAGE_REGISTER_SUCCES"]);
          this.nav.setRoot('LoginPage');
        },
        (error) => {
          console.log(error);
          this.loading.dismiss();
          this.createToast(this.translatedItems["MESSAGE_ERROR"]);
        }
      )
  }

  login() {
    this.nav.setRoot('LoginPage');
  }

  openTerms() {
    let forgot = this.termsAlert.create({
      title: this.translatedItems["TERMS"] + ".",
      message: this.translatedItems["TERMS_TEXT"] + ".",
      buttons: [
        {
          text: this.translatedItems["DECLINE"],
          handler: data => {
            this.registerForm.patchValue({ 'terms': false });
          }
        },
        {
          text: this.translatedItems["ACCEPT"],
          handler: data => {
            this.account.terms = true;
          }
        }
      ]
    });
    forgot.present();
  }

}
