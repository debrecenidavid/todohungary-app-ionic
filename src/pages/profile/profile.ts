import { Component } from '@angular/core';
import { IonicPage,App, NavController, LoadingOptions, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Profile } from '../../types/types';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  loading;

  // firstName:string;
  // lastName:string;
  // email:string;
  // date:Date;
  // gender:string;

  currentProfile:any={}

  public profile_segment:string;


  constructor(public app:App,public loadingCtrl: LoadingController, public navCtrl: NavController, private storage: Storage, public navParams: NavParams) {
  }

  ionViewWillEnter() {
    this.profile_segment = 'grid';
    this.getCurrentProfile();
  }
  presentLoading() {
    const opts: LoadingOptions = {
      content: 'Please wait...',
      dismissOnPageChange: true
    }
    this.loading = this.loadingCtrl.create(opts)
    this.loading.present()
  }

  async getCurrentProfile() {
    this.presentLoading();
    try {
      const res = await this.storage.get('user');
      this.loading.dismiss();
      this.currentProfile.email=res.email;
      this.currentProfile.lastName=res.lastName;
      this.currentProfile.firstName=res.firstName;
      this.currentProfile.date=new Date(res.date)

    } catch (error) {
      console.error(error);
      this.loading.dismiss();
    }
  }

  followingClicked(){
    console.log("asdas");
    
  }


  logout() {
    this.storage.set('token', null)
      .then(() => 
      // this.app.getRootNav().setRoot('LoginPage')
      window.location.reload()
      )
      .catch(err => console.error(err))
  }

  goEditProfile() {
    // Open it as a modal page
    // let modal = this.modalCtrl.create(EditProfile);
    // modal.present();
  }

  editProfile() {
    this.navCtrl.push('EditProfilePage',{profile:this.currentProfile})
    
  }

  goTaggedProfile() {
    // this.navCtrl.push(TaggedProfile);
  }

  goSavedProfile() {
    // this.navCtrl.push(SavedProfile);
  }

  // Triggers when user pressed a post
  pressPhoto(user_id: number, username: string, profile_img: string, post_img: string) {
    this.presentModal(user_id, username, profile_img, post_img);
  }

  // Set post modal
  presentModal(user_id: number, username: string, profile_img: string, post_img: string) {
    // let modal = this.modalCtrl.create(ModalPost, 
    // { // Send data to modal
    //   user_id: user_id, 
    //   username: username,
    //   profile_img: profile_img,
    //   post_img: post_img
    // }, // This data comes from API!
    // { showBackdrop: true, enableBackdropDismiss: true });
    // modal.present();
  }

}
