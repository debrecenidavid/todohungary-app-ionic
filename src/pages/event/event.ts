import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OfficalEvent } from '../../models/event';
import * as moment from 'moment';
import L,{ Map, MarkerOptions } from 'leaflet';
import { EventProvider } from '../../providers/event/event';
moment.locale('hu');

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {

  @ViewChild('minimap') mapContainer: ElementRef;
  minimap: Map;
  event: OfficalEvent;
  constructor(private eventProvider:EventProvider, public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewDidEnter() {
    // this.event = this.navParams.get('event');
    this.loadMap();
  }
  async loadMap() {
   this.event = await this.navParams.get('event');
    if (!this.minimap) {
      this.minimap = L.map('minimap', { zoomControl: false }).setView([this.event.location.lat*1.000025,this.event.location.lng], 15);
      L.tileLayer("http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}", {
        maxZoom: 18,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
      }).addTo(this.minimap);
      let icon = L.icon({
        iconUrl: this.eventProvider.getIconUrl(this.event.tags[0]),
        iconSize: [53, 80],
        iconAnchor: [27, 78],
        popupAnchor: [0, -79],
        // popupAnchor: [0, 0],
      })
      const markerOptions: MarkerOptions = {
        icon: icon,
      }
      L.marker([this.event.location.lat,this.event.location.lng], markerOptions).addTo(this.minimap);
    }
  }

  // ionViewDidLoad() {
  //   this.event = this.navParams.get('event');
  //   this.loadMap();
  // }

  getTimeLeft(startTs:string):string{
    return moment(startTs).fromNow();
  }

  navigate(){
    console.log('navigate');
    window.open(`https://www.google.com/maps/search/?api=1&query=${this.event.location.lat},${this.event.location.lng}`, '_blank');
    
  }

}
