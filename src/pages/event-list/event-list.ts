import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { OfficalEvent } from '../../models/event';
import { EventProvider } from '../../providers/event/event';
import * as moment from 'moment';
moment.locale('hu');

@IonicPage()
@Component({
  selector: 'page-event-list',
  templateUrl: 'event-list.html',
})
export class EventListPage {

  OfficialEvents: OfficalEvent[];
  filteredEvents: OfficalEvent[];

  dateFilter: string = 'Today';

  categoryFilter: string = 'All Category';

  show: string = 'all'
  
  constructor(private alertCtrl: AlertController, private event: EventProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  getTimeLeft(startTs:string):string{
     return moment(startTs).fromNow();
   }

   doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

   getFormattedDate(startTs:number,endTs:number):string{
     console.log(startTs);
     console.log(moment(startTs).format('MMM DD HH:mm'));
     console.log(moment(startTs).isSame(moment(endTs),'day'));
     if(moment(startTs).isSame(moment(endTs),'day')){
       return `${moment(startTs).format('MMM. DD HH:mm').toUpperCase()} - ${moment(endTs).format('HH:mm')}`
     }
     
     return `${moment(startTs).format('MMM. DD HH:mm')} - ${moment(endTs).format('DD HH:mm')}`
   }
  getFilteredEvents() {
    this.filteredEvents = this.event.getEventsForDates(this.OfficialEvents, this.dateFilter);
    if (this.categoryFilter !== 'All Category') {
      this.filteredEvents = this.event.getEventsForTags(this.filteredEvents, this.categoryFilter);
    }
    this.filteredEvents = this.filteredEvents.map((ev:OfficalEvent)=>{
      ev.formattedDate=this.getFormattedDate(ev.startTs,ev.endTs);
      return ev;
    })
    console.log(this.filteredEvents);
    
  }
  ionViewDidLoad() {
    this.getEvents();
  }

  openEvent(event){
    console.log(event);
    this.navCtrl.push('EventPage',{event})
    
  }

  async getEvents() {
    try {
      this.OfficialEvents = await this.event.getOfficialEvents();
      this.getFilteredEvents();
    } catch (err) {
      console.error(err)
    }
  }

  segmentChanged(){
    console.log(event);
    
  }

}
