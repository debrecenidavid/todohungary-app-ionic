import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root = 'EventMapPage';
  tab2Root = 'EventListPage';
  tab3Root = 'ProfilePage';

  tab4Root='SettingPage'

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  console.log('tab constructor called');
  
  }

}
