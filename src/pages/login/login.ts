import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, ToastController, LoadingController, LoadingOptions } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  translatedItems;

  loading;
  loginForm: FormGroup;
  constructor(public nav: NavController,
    public navParams: NavParams,
    public translateService: TranslateService,
    public forgotCtrl: AlertController,
    public toastCtrl: ToastController,
    public menu: MenuController,

    private storage: Storage,

    private userProvider: UserProvider,

    public loadingCtrl: LoadingController
  ) {
    const itemsToTranslate: string[] = ["FORGOT_PASSWORD","MESSAGE_ERROR", "MESSAGE_FORGOT_PASSWORD",
      "CANCEL", "MESSAGE_EMAIL_SENDED", "SEND"]

    translateService.get(itemsToTranslate).subscribe(val => {
      this.translatedItems = val;
    })

    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    })

  }
  presentLoading() {
    const opts:LoadingOptions={
      content: 'Please wait...',
      dismissOnPageChange:true
    }
    this.loading= this.loadingCtrl.create(opts)
    this.loading.present()
}
createToast(message:string){
  let toast = this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'top',
    cssClass: 'dark-trans',
    closeButtonText: 'OK',
    showCloseButton: true
  });
  toast.present();
}

  ionViewWillEnter() {
    this.getCurrentUser();
  }

  async getCurrentUser() {
    this.presentLoading();
    await this.storage.get('loginDetails')
    .then((val:any)=>{
      if(val){
        this.loginForm.patchValue({'email':val.email})
        this.loginForm.patchValue({'password':val.password})
      }
    })
    .catch(err=>err)
    this.storage.get('token').then(token => {
      if (token) {
        this.userProvider.getUser(token)
          .subscribe(
            (res) => {
              this.storage.set('user', res);
              this.nav.setRoot('TabsPage');
            },
            (error) => {
            }
          )
      }
      this.loading.dismiss();
    })
  }
  register() {
    this.presentLoading();
    this.nav.setRoot('RegisterPage');
  }

  async login() {
    this.presentLoading();
    await this.storage.set('loginDetails',{email:this.loginForm.value.email,password:this.loginForm.value.password})
    this.userProvider.login(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe(
        (res: any) => {
          this.storage.set('token', res.token);
          this.nav.setRoot('TabsPage');
        },
        (error) => {
          this.loading.dismiss();
          this.createToast(this.translatedItems["MESSAGE_ERROR"]);
        }
      )
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: this.translatedItems["FORGOT_PASSWORD"] + "?",
      message: this.translatedItems["MESSAGE_FORGOT_PASSWORD"] + ".",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email',
        },
      ],
      buttons: [
        {
          text: this.translatedItems["CANCEL"],
          handler: data => {
          }
        },
        {
          text: this.translatedItems["SEND"],
          handler: data => {
            this.createToast(this.translatedItems["MESSAGE_EMAIL_SENDED"]);
          }
        }
      ]
    });
    forgot.present();
  }

}
