import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import L, { MarkerOptions, FeatureGroup, Map, } from 'leaflet';
import { EventProvider } from '../../providers/event/event';
import { OfficalEvent, LocationEv } from '../../models/event';
import { DATE_FILTERS } from '../../mocks/dateFilters';
import { CATEGORY_FILTERS } from '../../mocks/categoryFilters';
import * as moment from 'moment';
moment.locale('hu');

@IonicPage()
@Component({
  selector: 'page-event-map',
  templateUrl: 'event-map.html',
})
export class EventMapPage {
  @ViewChild('map') mapContainer: ElementRef;
  map: Map;

  OfficialEvents: OfficalEvent[];
  markerGroup: FeatureGroup;
  show: string = 'all'

  dateFilter: string = 'Today';

  categoryFilter: string = 'All Category'

  constructor(private alertCtrl: AlertController, private event: EventProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  getFilteredEvents() {
    let events: OfficalEvent[] = this.event.getEventsForDates(this.OfficialEvents, this.dateFilter);
    if (this.categoryFilter !== 'All Category') {
      events = this.event.getEventsForTags(events, this.categoryFilter);
    }
    this.addMarkers(events);
  }

  ionViewDidEnter() {
    this.getEvents();
    this.loadMap();
  }
  loadMap() {
    if (!this.map) {
      this.map = L.map('map', { zoomControl: false }).setView([47.53333, 21.63333], 13);
      L.tileLayer("http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}", {
        maxZoom: 18,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
      }).addTo(this.map);
    }
  }

  addMarkers(events: OfficalEvent[]) {
    this.markerGroup ? this.markerGroup.removeFrom(this.map) : '';
    this.markerGroup = L.featureGroup();
    let locationNames = []
    events.sort((a, b) => a.startTs > b.startTs ? 1 : -1);
    const asd: LocationEv[] = events.reduce((acc, event) => {
      acc[event.location.name] = acc[event.location.name] || Object.assign(event.location, {
        events: []
      });
      acc[event.location.name].events.push(event)
      if (locationNames.indexOf(event.location.name) === -1) locationNames.push(event.location.name);
      return acc;
    }, {} as LocationEv[])
    const locationsEvents = Object.keys(asd).map(
      name => asd[name]
    );

    locationsEvents.map((locationEvent: LocationEv) => {
      let icon = L.icon({
        iconUrl: this.categoryFilter === 'All Category' ? this.event.getIconUrl(locationEvent.events[0].tags[0]) : this.event.getIconUrl(this.categoryFilter),
        iconSize: [53, 80],
        iconAnchor: [27, 78],
        popupAnchor: [0, -79],
        // popupAnchor: [0, 0],
      })
      const markerOptions: MarkerOptions = {
        icon: icon,
      }
      let popUp = L.popup({
        className: 'pUp'
      }).setContent(
        locationEvent.events.map((e, i) => {
          return `
          ${i === 0 ? e.location.name : ''}<br/>
          <div key=${e.name}><a target="blank" href=${e.link} >
          <img src=${e.imgurl ? e.imgurl : 'https://i.kinja-img.com/gawker-media/image/upload/Image_Not_Found_1x_qjofp8.png'} width="200" height="120"/><br/>
          ${e.name}<br/>
         ${moment(e.startTs).format('YYYY-MM-DD HH:mm')} - ${moment(e.endTs).format('YYYY-MM-DD HH:mm')}
          </a>
          </div>`
        }).join(''));
      L.marker([locationEvent.lat, locationEvent.lng], markerOptions).addTo(this.markerGroup).bindPopup(popUp);
    });
    this.markerGroup.addTo(this.map);
  }

  async getEvents() {
    try {
      this.OfficialEvents = await this.event.getOfficialEvents();
      this.getFilteredEvents();
    } catch (err) {
      console.error(err)
    }
  }

  segmentChanged() {
    console.log(this.show);
  }

  dateSelect() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Date');

    DATE_FILTERS.map(date => {
      alert.addInput({
        type: 'radio',
        label: date.name,
        value: date.name,
        checked: date.name === this.dateFilter
      })
    })
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: (data: any) => {
        console.log('Radio data:', data);
        this.dateFilter = data;
        this.getFilteredEvents()
      }
    });

    alert.present();
  }

  categorySelect() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Date');

    CATEGORY_FILTERS.map(cat => {
      alert.addInput({
        type: 'radio',
        label: cat.name,
        value: cat.name,
        checked: cat.name === this.categoryFilter
      })
    })
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: (data: any) => {
        console.log('Radio data:', data);
        this.categoryFilter = data;
        this.getFilteredEvents()
      }
    });

    alert.present();
  }
}
